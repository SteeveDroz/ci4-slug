<?php

if (!function_exists('helper')) {
    function helper($file)
    {
        require_once('vendor/codeigniter4/framework/system/Helpers/' . $file . '_helper.php');
    }
}
