<?php

namespace SteeveDroz\CiSlug;

use CodeIgniter\Model;
use Normalizer;

/**
 * This class simplifies the creation of a slug to store in a database.
 */
class Slugify
{
    /**
     * The model to use when creating the slug
     */
    protected Model $model;
    /**
     * The default value of the slug field
     */
    protected string $slugField = 'slug';
    /**
     * Whether the diactritics need to be stripped from the slug
     */
    protected bool $stripDiacritics;

    /**
     * This constructor sets up the calculation of the slug.
     * 
     * @param Model $model a CodeIgniter 4 Model to study for the slug creation
     * @param bool $stripDiacritics Whether the diacritics need to be stripped
     */
    public function __construct(Model $model, bool $stripDiacritics = false)
    {
        helper('text');
        $this->model = $model;
        $this->stripDiacritics = $stripDiacritics;
    }

    /**
     * This setter allows to change the name of the field that will store the slug.
     * 
     * The default name is "slug".
     * 
     * @param string $fieldName The new name for the slug field
     */
    public function setField(string $fieldName): void
    {
        $this->slugField = $fieldName;
    }

    /**
     * This is the main method, it creates a slug and adds it to the given array before returning it.
     * 
     * The slug is first calculated, then the Model is called to check if the slug already exists. If it's the case, `-2` is added to the slug and the presence is checked one more time.
     * 
     * If it still exists in the database, `-3` is added instead of `-2` and the presence is checked again, replacing the suffix until the slug is unique.
     * 
     * When a unique slug is found, it is added to the data passed as argument and returned.
     * 
     * If $stripDiactitics was set to `true` in the constructor, all the diactritics are stripped from the text and replaced by their corresponding letter.
     * 
     * @param array $data The data as formatted by the model when using hooks as $beforeInsert.
     * @param string $nameField The name of the field to sluggify.
     * @return array The input $data with an additionnal parameter containing the slug.
     */
    public function addSlug(array $data, string $nameField): array
    {
        if (!isset($data['data'][$nameField])) {
            return $data;
        }

        $currentId = $data['id'][0] ?? -1;

        $slug = $data['data'][$nameField];
        if ($this->stripDiacritics) {
            $slug = str_replace('œ', 'oe', $slug);
            $slug = str_replace('æ', 'ae', $slug);
            $slug = str_replace('Œ', 'oe', $slug);
            $slug = str_replace('Æ', 'ae', $slug);
            $slug = Normalizer::normalize($slug, Normalizer::NFKD);
            $firstDiacritics = "\u{0300}";
            $lastDiacritics = "\u{036f}";
            $slug = preg_replace("/[$firstDiacritics-$lastDiacritics]/", '', $slug);
        }

        $slug = \url_title($slug, '-', true);
        $entry = $this->model->where($this->slugField, $slug)->withDeleted()->first();

        while (null !== $entry && array_key_exists('id', $entry) && $entry['id'] != $currentId) {
            $slug = \increment_string($slug, '-', 2);
            $entry = $this->model->where($this->slugField, $slug)->withDeleted()->first();
        }

        $data['data'][$this->slugField] = $slug;

        return $data;
    }
}
